import numpy as np

ENG = 'abcdefghijklmnopqrstuvwxyz'


def substitute_with_eng(str, alpha):
    iter = 0
    alphabet = {}
    for sym in str:
        if sym not in alphabet:
            alphabet[sym] = alpha[iter]
            iter += 1

    res = ''
    for sym in str:
        res += alphabet[sym]
    return res


def mod_matrix_inverse(A, p):  # Finds the inverse of matrix A mod p
    n = len(A)
    A = np.array(A)
    adj = np.zeros(shape=(n, n))
    for i in range(0, n):
        for j in range(0, n):
            adj[i][j] = ((-1) ** (i + j) * int(round(np.linalg.det(minor(A, j, i))))) % p
    return (mod_inverse(int(round(np.linalg.det(A))), p) * adj) % p


def mod_inverse(a, p):  # Finds the inverse of a mod p, if it exists
    for i in range(1, p):
        if (i * a) % p == 1: return i
    raise ValueError(str(a) + " has no inverse mod " + str(p))


def minor(A, i, j):  # Return matrix A with the ith row and jth column deleted
    A = np.array(A)
    minor = np.zeros(shape=(len(A) - 1, len(A) - 1))
    p = 0
    for s in range(0, len(minor)):
        if p == i:
            p = p + 1
        q = 0
        for t in range(0, len(minor)):
            if q == j: q = q + 1
            minor[s][t] = A[p][q]
            q = q + 1
        p = p + 1
    return minor


class HillCipher:
    def __init__(self, alphabet):
        self.alphabet = {alphabet[i]: i for i in range(len(alphabet))}
        self.reverse_alphabet = {i: alphabet[i] for i in range(len(alphabet))}
        self.modulo = 26

    def _get_key_matrix(self, key):
        return [
            [self.alphabet[key[0]], self.alphabet[key[1]]],
            [self.alphabet[key[2]], self.alphabet[key[3]]]
        ]

    def _split_to_blocks(self, text, l):
        res = []
        for i in range(0, len(text) - l + 1, l):
            res.append([self.alphabet[sym] for sym in text[i: i + l]])
        return res

    def encrypt(self, plaintext, key):
        key_matrix = np.array(self._get_key_matrix(key))
        plain_blocks = self._split_to_blocks(plaintext, len(key_matrix))

        cipher_blocks = [np.dot(key_matrix, np.array(block)) for block in plain_blocks]

        res = ''
        for block in cipher_blocks:
            for code in block:
                res += self.reverse_alphabet[code % self.modulo]

        return res

    def decrypt(self, ciphertext, key):
        key_matrix = mod_matrix_inverse(self._get_key_matrix(key), 26)
        cipher_blocks = self._split_to_blocks(ciphertext, len(key_matrix))
        plain_blocks = [np.dot(key_matrix, block) for block in cipher_blocks]

        res = ''
        for block in plain_blocks:
            for code in block:
                res += self.reverse_alphabet[code % self.modulo]

        return res


def main():
    hill = HillCipher(alphabet=ENG)
    print(hill.encrypt('lokeqw', 'afjk'))
    print(hill.decrypt('sfuaga', 'afjk'))


if __name__ == '__main__':
    main()
